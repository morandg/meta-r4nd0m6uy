DESCRIPTION = "Base image for charguychess DIY hardware"

inherit core-image extrausers

IMAGE_INSTALL = " \
  packagegroup-core-boot \
  charguychess \
  i2c-tools \
  bash \
  sudo \
  wpa-supplicant \
  kernel-module-i2c-dev \
  "

IMAGE_INSTALL_append_charguychess-rpi3 = " \
  kernel-module-i2c-bcm2835 \
  kernel-module-brcmfmac \
  linux-firmware-rpidistro-bcm43430 \
  "

IMAGE_FEATURES = "ssh-server-dropbear"

EXTRA_USERS_PARAMS = " \
  usermod -P charguychess root; \
  useradd -P charguychess charguychess; \
  "
