SUMMARY = "CppUTest xUnit test framework"
DESCRIPTION = "CppUTest is a C/C++ based unit xUnit test framework for unit testing."
HOMEPAGE = "http://cpputest.github.io/"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING;md5=ce5d5f1fe02bcd1343ced64a06fd4177"

FILES_${PN}-dev += "${libdir}/CppUTest"

ALLOW_EMPTY_${PN} = "1"

inherit cmake

BBCLASSEXTEND = "nativesdk"

EXTRA_OECMAKE = "-DTESTS=OFF"

SRC_URI = " \
  git://github.com/cpputest/cpputest.git;tag=v${PV} \
  "

S = "${WORKDIR}/git"
