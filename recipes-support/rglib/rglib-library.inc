include rglib.inc

DEPENDS += " \
  libevent \
  log4cplus \
  "

do_compile() {
  oe_runmake static LDFLAGS="${LDFLAGS}" DEBUG=1 V=1
  oe_runmake shared LDFLAGS="${LDFLAGS}" DEBUG=1 V=1
}

do_install() {
  oe_runmake install DESTDIR=${D} PREFIX=${prefix} DEBUG=1 V=1
}
