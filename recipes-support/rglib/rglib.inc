SUMMARY = "R4nd0m6uy library"
DESCRIPTION = "Reusable C++ objects for platforms abstraction"
AUTHOR = "R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>"
HOMEPAGE = "https://gitlab.com/morandg/lib-r4nd0m6uy"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"
DEPENDS = " \
  pkgconfig-native \
  "

BBCLASSEXTEND = "nativesdk"

RGLIB_BRANCH ?= "master"
RGLIB_GIT_URL ?= "git://gitlab.com/morandg/lib-r4nd0m6uy.git;protocol=https;branch=${RGLIB_BRANCH}"

SRC_URI = " \
  ${RGLIB_GIT_URL} \
  "

S = "${WORKDIR}/git"
