require rglib-library.inc

SUMMARY += " mock objects"

DEPENDS += " \
  cpputest \
  "

RDEPENDS_${PN}-staticdev += " \
  cpputest-staticdev \
  "

ALLOW_EMPTY_${PN} = "1"

SRCREV = "${AUTOREV}"
PV = "git-${SRCPV}"

do_compile() {
  oe_runmake mockslib LDFLAGS="${LDFLAGS}" DEBUG=1 V=1
}

do_install() {
  oe_runmake install_mocks DESTDIR=${D} PREFIX=${prefix} DEBUG=1 V=1
}
