SUMMARY = "Octochess is a free open source chess engine"
DESCRIPTION = "Octochess is a free open source chess engine"
AUTHOR = "Tim Kosse"
HOMEPAGE = "http://octochess.org/"
SECTION = "games"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

PR = "r0"

SRCREV = "${AUTOREV}"

SRC_URI = " \
  svn://svn.filezilla-project.org/svn/;module=chess;protocol=https \
  file://fix_cross_compilation.patch \
"

S="${WORKDIR}/chess"

do_compile() {
  oe_runmake octochess
}

do_install() {
  install -D -m 0755 ${S}/octochess ${D}/${bindir}/octochess
}
