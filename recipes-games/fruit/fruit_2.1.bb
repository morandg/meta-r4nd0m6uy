SUMMARY = "Fruit is a chess engine developed by Fabien Letouzey"
DESCRIPTION = "Fruit is a chess engine developed by Fabien Letouzey"
AUTHOR = "Fabien Letouzey"
HOMEPAGE = "http://arctrix.com/nas/chess/fruit/"
SECTION = "games"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://copying.txt;md5=fdafc691aa5fb7f8e2a9e9521fef771b"

PR = "r0"

SRC_URI = " \
  http://arctrix.com/nas/chess/fruit/fruit_21_linux.zip \
  file://fix_cross_compile.patch \
"
SRC_URI[md5sum] = "35e538708a036f58aa88a7acee6a2030"
SRC_URI[sha256sum] = "ad13f6099dc2acebf0112c36cc7d38fd4009316ad60ecc294c5e828380dcd2c0"

S = "${WORKDIR}/fruit_21_linux/"

do_compile() {
  cd src/ && oe_runmake
}

do_install() {
  install -D ${S}/src/fruit ${D}/${bindir}/fruit
}
