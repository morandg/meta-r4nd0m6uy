SUMMARY = "Software for the Charguychess DIY hardware"
DESCRIPTION = "Software for the Charguychess DIY hardware"
AUTHOR = "R4nd0m6uy and Charly"
HOMEPAGE = "https://gitlab.com/morandg/charguychess2"
SECTION = "games"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d32239bcb673463ab874e80d47fae504"
DEPENDS = " \
  pkgconfig-native \
  libconfig \
  rglib \
  "
RRECOMMENDS_${PN} = " \
  charguychess-driver \
  stockfish \
  fruit \
  octochess \
  "

inherit update-rc.d

CGC_BRANCH ?= "master"
CGC_GIT_URL ?= "git://gitlab.com/morandg/charguychess2.git;protocol=https;branch=${CGC_BRANCH}"

SRC_URI = " \
  ${CGC_GIT_URL} \
  file://config \
  file://sysvinit \
  file://log4cplus.ini \
  "

S = "${WORKDIR}/git"

INITSCRIPT_NAME = "charguychess"
INITSCRIPT_PARAMS = "defaults 99"

do_compile() {
  oe_runmake -C app LDFLAGS="${LDFLAGS}" DEBUG=1 V=1
}

do_install() {
  oe_runmake install -C app DESTDIR=${D} DEBUG=1 V=1 PREFIX=${prefix}

  install -D -m 0664 ${WORKDIR}/config ${D}${sysconfdir}/charguychess/config
  install -D -m 0664 ${WORKDIR}/log4cplus.ini ${D}${sysconfdir}/charguychess/logging.ini
  install -D -m 0755 ${WORKDIR}/sysvinit ${D}${sysconfdir}/init.d/charguychess
}
