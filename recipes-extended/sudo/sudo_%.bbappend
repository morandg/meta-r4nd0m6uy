FILESEXTRAPATHS_prepend := "${THISDIR}/files/:"

SRC_URI += " \
  file://charguychess \
  "

do_install_append() {
  install -D -m 0440 ${WORKDIR}/charguychess ${D}${sysconfdir}/sudoers.d/charguychess
}
