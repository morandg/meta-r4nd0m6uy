FILESEXTRAPATHS_prepend := "${THISDIR}/files/:"

SRC_URI += " \
  file://r4nd0m6uy.sh \
  "

do_install_append() {
  install -D -m 0644 ${WORKDIR}/r4nd0m6uy.sh ${D}${sysconfdir}/profile.d/r4nd0m6uy.sh
}

FILES_${PN} += " \
  ${sysconfdir}/profile.d/r4nd0m6uy.sh \
  "
