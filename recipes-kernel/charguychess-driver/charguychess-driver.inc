SUMMARY = "Charguychess kernel driver"
DESCRIPTION = "Kernel driver to communicate with the charguychess hardware"
AUTHOR = "R4nd0m6uy <r4nd0m6uy@r4nd0m6uy.ch>"
HOMEPAGE = "https://gitlab.com/morandg/charguychess2"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b234ee4d69f5fce4486a80fdaf4a4263"

inherit module

CGC_DRIVER_BRANCH ?= "master"
CGC_DRIVER_GIT_URL ?= "git://gitlab.com/morandg/charguychess2.git;protocol=https;branch=${CGC_DRIVER_BRANCH}"

SRC_URI = "\
  ${CGC_DRIVER_GIT_URL} \
  "

S = "${WORKDIR}/git/kernel/driver"

KERNEL_MODULE_AUTOLOAD = "charguychess"
